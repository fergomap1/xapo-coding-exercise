<h1 align="center">
  Xapo coding exercise
</h1>

<h3 align="center">
Web app developed with React + Typescript. It shows a list of cryptocurrencies with their USD value and a simple calculator to exchange dollars to any of the available currencies.
</h3>
<br>

# Hosting

The project is hosted at https://xapo-coding-exercise-fergomap.netlify.app.

# Set up
Node installed, in my case I have the following versions:
| Name | Version |
| - | - |
| Node | 14.15.5 |
| npm | 6.14.11 |

In the root of the project run:

```
npm install
```

# Scripts

| Script | Description |
| - | - |
| `npm run start` | Run the project. |
| `npm run build` | Create the production build. |
| `npm run deploy` | Create the production build and deploy the app to the firebase hosting.  |
| `npm run lint` | Run ESLint to check if the code has any problem. |
| `npm run test` | Run jest to test all the project. |
| `npm run test:watch` | Run jest to test all the project and keeps watching for updates. |
| `npm run test:coverage` | Run jest to test all the project and generates a coverage report in /coverage/Icov-report/index.html. |

# Other options

- Redux is very popular in React apps but it makes no sense in this small app.
- React hook form is very useful in forms handling but this app is too simple to add it.
- An styling library like Bootstrap would be nice, but it has too many options for only two views.
