export default interface RatesInformation {
    rates: Record<string, number>;
    timestamp: number;
}
