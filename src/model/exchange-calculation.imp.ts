/* istanbul ignore file */

import ExchangeCalculation from './exchange-calculation';

export default class ExchangeCalculationImp implements ExchangeCalculation {
    createdAtTimestamp: number;
    currency: string;
    currencyAmount: number;
    dollarAmount: number;
    timestamp: number;

    constructor(createdAtTimestamp: number = 0, currency: string = '', currencyAmount: number = 0, dollarAmount: number = 0, timestamp: number = 0) {
        this.createdAtTimestamp = createdAtTimestamp;
        this.currency = currency;
        this.currencyAmount = currencyAmount;
        this.dollarAmount = dollarAmount;
        this.timestamp = timestamp;
    }
}
