export default interface ExchangeCalculation {
    createdAtTimestamp: number;
    currency: string;
    currencyAmount: number;
    dollarAmount: number;
    timestamp: number;
}
