/* istanbul ignore file */

import SelectOption from './select-option';

export default class SelectOptionImp implements SelectOption {
    value: string;
    label: string;

    constructor(value: string = '', label: string = '') {
        this.value = value;
        this.label = label;
    }
}
