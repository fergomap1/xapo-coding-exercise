/* istanbul ignore file */

import RatesInformation from './rates-information';

export default class RatesInformationImp implements RatesInformation {
    rates: Record<string, number>;
    timestamp: number;

    constructor(rates: Record<string, number> = {}, timestamp: number = 0) {
        this.rates = rates;
        this.timestamp = timestamp;
    }
}
