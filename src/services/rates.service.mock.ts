/* istanbul ignore file */

import { SynchronousPromise } from 'synchronous-promise';
import RatesInformationImp from 'model/rates-information.imp';

export const mockRatesService = (service: any): void => {
    service.getRates = jest.fn().mockReturnValue(SynchronousPromise.unresolved());
};
