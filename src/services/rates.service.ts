import axios, { AxiosResponse } from 'axios';
import { APP_CONSTANTS } from 'config/app.config';
import RatesInformation from 'model/rates-information';
import RatesInformationImp from 'model/rates-information.imp';

export const getRates = (): Promise<RatesInformation> => {
    return axios.get(APP_CONSTANTS.ENDPOINTS.RATES)
        .then((response: AxiosResponse) => new RatesInformationImp(response.data.rates, response.data.timestamp));
};
