import { getRates } from './rates.service';
import axios from 'axios';
import { APP_CONSTANTS } from 'config/app.config';
import RatesInformation from 'model/rates-information';
import RatesInformationImp from 'model/rates-information.imp';
import { SynchronousPromise } from 'synchronous-promise';

describe('RatesService', () => {
    describe('getRates', () => {
        it('should call to axios.get and return a promise with the data parsed as a RatesInformation object', () => {
            axios.get = jest.fn().mockReturnValue(SynchronousPromise.resolve({ data: { rates: { 'BTC': 1 }, timestamp: 10 } }));

            return getRates().then((ratesInformationResponse: RatesInformation) => {
                expect(ratesInformationResponse).toEqual(new RatesInformationImp({ 'BTC': 1 }, 10));
                expect(axios.get).toHaveBeenCalledWith(APP_CONSTANTS.ENDPOINTS.RATES);
            });
        });
    });
});