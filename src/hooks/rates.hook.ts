import { useEffect, useState } from 'react';
import { AxiosError } from 'axios';
import { getRates } from 'services/rates.service';
import RatesInformation from 'model/rates-information';
import RatesInformationImp from 'model/rates-information.imp';

interface RatesHookInfo {
    errorMessage: string;
    isLoading: boolean;
    ratesInformation: RatesInformation;
}

const useRates = (): RatesHookInfo => {
    const [ errorMessage, setErrorMessage ] = useState<string>('');
    const [ isLoading, setIsLoading ] = useState<boolean>(false);
    const [ ratesInformation, setRatesInformation ] = useState<RatesInformation>(new RatesInformationImp());

    useEffect(() => {
        setIsLoading(true);

        getRates()
            .then((ratesInformationResponse: RatesInformation) => setRatesInformation(ratesInformationResponse))
            .catch((error: AxiosError) => setErrorMessage(error?.response?.data?.description || 'Something failed, try again later.'))
            .finally(() => setIsLoading(false));
    }, []);

    return { errorMessage, isLoading, ratesInformation };
};

export default useRates;
