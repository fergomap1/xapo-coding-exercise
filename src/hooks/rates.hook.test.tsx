import React, { FunctionComponent, ReactElement } from 'react';
import { render } from '@testing-library/react';
import * as ratesService from 'services/rates.service';
import { mockRatesService } from 'services/rates.service.mock';
import { SynchronousPromise } from 'synchronous-promise';
import useRates from './rates.hook';
import { act } from 'react-dom/test-utils';
import RatesInformationImp from 'model/rates-information.imp';

const TestComponent: FunctionComponent = (): ReactElement => {
    const { errorMessage, isLoading, ratesInformation } = useRates();
    
    return <div>
        { errorMessage }
        { isLoading && 'loading' }
        { ratesInformation.timestamp ? 'rates' : '' }
    </div>;
};

describe('useRates hook', () => {

    beforeEach(() => {
        mockRatesService(ratesService);
    });

    it('should show loading first, return the rates object and hide loading when the getRates works', () => {
        const getRatesPromise = SynchronousPromise.unresolved();
        (ratesService.getRates as jest.Mock).mockReturnValue(getRatesPromise);

        const { container } = render(<TestComponent/>);

        expect(container.textContent).toEqual('loading');

        act(() => {
            getRatesPromise.resolve(new RatesInformationImp({}, 1));
        });

        expect(container.textContent).toEqual('rates');
    });

    it('should show loading first, return a detailed error message when it is defined and hide loading when the getRates fails', () => {
        const getRatesPromise = SynchronousPromise.unresolved();
        (ratesService.getRates as jest.Mock).mockReturnValue(getRatesPromise);

        const { container } = render(<TestComponent/>);

        expect(container.textContent).toEqual('loading');

        act(() => {
            getRatesPromise.reject({ response: { data: { description: 'Bad request!' }}});
        });

        expect(container.textContent).toEqual('Bad request!');
    });

    it('should show loading first, return a generic error message when the error is not defined and hide loading when the getRates fails', () => {
        const getRatesPromise = SynchronousPromise.unresolved();
        (ratesService.getRates as jest.Mock).mockReturnValue(getRatesPromise);

        const { container } = render(<TestComponent/>);

        expect(container.textContent).toEqual('loading');

        act(() => {
            getRatesPromise.reject('');
        });

        expect(container.textContent).toEqual('Something failed, try again later.');
    });
});
