import React, { ChangeEvent } from 'react';
import '@testing-library/jest-dom/extend-expect';
import SelectOption from 'model/select-option';

interface SelectProps {
    onChange: Function;
    options: SelectOption[];
    value: SelectOption;
}

jest.mock('react-select', () => ({ options, value, onChange }: SelectProps) => {
    const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
        onChange(options.find(option => option.value === event.currentTarget.value));
    }

    return <select data-testid='select' value={value?.value} onChange={handleChange}>
        {options.map(({ label, value }) => <option key={value} value={value} >
            {label}
        </option>)}
    </select>;
});
