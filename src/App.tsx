import React, { FunctionComponent, ReactElement } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { APP_CONSTANTS } from 'config/app.config';
import ExchangeComponent from 'pages/exchange/exchange.component';
import HeaderComponent from 'components/header/header.component';
import LoadingComponent from 'components/loading/loading.component';
import RatesComponent from 'pages/rates/rates.component';
import useRates from 'hooks/rates.hook';

const App: FunctionComponent = (): ReactElement => {
    const { errorMessage, isLoading, ratesInformation } = useRates();

    return <BrowserRouter>
        {isLoading && <LoadingComponent />}
        <HeaderComponent/>
        <div className="container">
            {errorMessage && <p className="error-message" data-testid="error-message">{errorMessage}</p>}
            <Switch>
                <Route path={APP_CONSTANTS.ROUTES.EXCHANGE} exact>
                    <ExchangeComponent ratesInformation={ratesInformation}/>
                </Route>
                <Route path={APP_CONSTANTS.ROUTES.EXCHANGE_DETAIL}>
                    <ExchangeComponent ratesInformation={ratesInformation}/>
                </Route>
                <Route path={APP_CONSTANTS.ROUTES.RATES}>
                    <RatesComponent ratesInformation={ratesInformation} />
                </Route>
                <Redirect to={APP_CONSTANTS.ROUTES.RATES} />
            </Switch>
        </div>
    </BrowserRouter>;
}

export default App;
