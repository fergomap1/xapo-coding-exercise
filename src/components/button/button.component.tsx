import React, { FunctionComponent, ReactElement } from 'react';

interface ButtonComponentProps {
    label: string;
    onClick?: Function;
    type: 'button' | 'submit';
}

const ButtonComponent: FunctionComponent<ButtonComponentProps> = ({ label, onClick, type }): ReactElement => {
    return <button type={type} onClick={() => onClick && onClick()} data-testid="button">
        { label }
    </button>;
}

export default ButtonComponent;
