import React from 'react';
import { render } from '@testing-library/react';
import HeaderComponent from './header.component';
import { MemoryRouter } from 'react-router';

describe('HeaderComponent', () => {
    it('should render a header with 2 links', () => {
        const { container } = render(<MemoryRouter><HeaderComponent/></MemoryRouter>);

        expect(container.getElementsByTagName('a').length).toEqual(2);
    });
});
