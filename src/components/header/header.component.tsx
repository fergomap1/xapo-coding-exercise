import React, { FunctionComponent, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { APP_CONSTANTS } from 'config/app.config';
import './header.component.scss';

const HeaderComponent: FunctionComponent = (): ReactElement => {
    return <header className="header-component">
        <span className="title">Xapo Coding Exercise</span>
        <div>
            <Link to={APP_CONSTANTS.ROUTES.RATES} className="header-link">
                Rates
            </Link>
            <Link to={APP_CONSTANTS.ROUTES.EXCHANGE} className="header-link">
                Exchange
            </Link>
        </div>
    </header>;
};

export default HeaderComponent;
