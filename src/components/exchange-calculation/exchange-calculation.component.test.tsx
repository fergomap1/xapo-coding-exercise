import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ExhcangeCalculationComponent from './exchange-calculation.component';
import ExchangeCalculationImp from 'model/exchange-calculation.imp';
import { MemoryRouter } from 'react-router';

describe('ExhcangeCalculationComponent', () => {
    it('should render the timestamp of the currency value, a text with the exchange value calculation, a link to the currency and call to onDelete when the delete button is clicked', () => {
        const onDelete = jest.fn();
        const exchangeCalculation = new ExchangeCalculationImp(0, 'BTC', 1, 56000, 1619784000);

        const { getByTestId } = render(<MemoryRouter><ExhcangeCalculationComponent calculation={exchangeCalculation} onDelete={onDelete} /></MemoryRouter>);

        fireEvent.click(getByTestId('button'));

        expect(getByTestId('timestamp-currency').textContent).toEqual('With value of Fri, 30 Apr 2021 12:00:00 GMT');
        expect(getByTestId('calculation-info').textContent).toEqual('1.00000000 BTC are 56,000.00000000 USD');
        expect(getByTestId('currency-link')).toHaveAttribute('href', '/exchange/BTC');
        expect(onDelete).toHaveBeenCalledWith(exchangeCalculation);
    });
});
