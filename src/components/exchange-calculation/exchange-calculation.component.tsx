import React, { FunctionComponent, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import ExchangeCalculation from 'model/exchange-calculation';
import { APP_CONSTANTS } from 'config/app.config';
import ButtonComponent from 'components/button/button.component';
import './exchange-calculation.component.scss';

interface ExchangeComponentProps {
    calculation: ExchangeCalculation;
    onDelete: Function;
}

const ExchangeCalculationComponent: FunctionComponent<ExchangeComponentProps> = ({ calculation, onDelete }): ReactElement => {
    return <div className="exchange-calculation-component">
        <p className="calculation-title" data-testid="timestamp-currency">With value of {new Date(calculation.timestamp * APP_CONSTANTS.MILLISECONDS_IN_SECOND).toUTCString()}</p>
        <div className="calculation-body">
            <span className="calculation-info" data-testid="calculation-info">
                {calculation.currencyAmount.toLocaleString('en-US', { maximumFractionDigits: 8, minimumFractionDigits: 8 })}
                <Link className="currency-link" data-testid="currency-link" to={`${APP_CONSTANTS.ROUTES.EXCHANGE}/${calculation.currency}`}> {calculation.currency} </Link>
                are {calculation.dollarAmount.toLocaleString('en-US', { maximumFractionDigits: 8, minimumFractionDigits: 8 })} USD
            </span>
            <ButtonComponent label="DELETE" onClick={() => onDelete(calculation)} type="button" />
        </div>
    </div>;
};

export default ExchangeCalculationComponent;
