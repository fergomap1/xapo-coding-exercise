import React from 'react';
import { render } from '@testing-library/react';
import LoadingComponent from './loading.component';

describe('LoadingComponent', () => {
    it('should render a loading spinner', () => {
        const { getByTestId } = render(<LoadingComponent/>);

        expect(getByTestId('loading-spinner')).toBeDefined();
    });
});
