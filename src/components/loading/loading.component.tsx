import React, { FunctionComponent, ReactElement } from 'react';
import './loading.component.scss';

const LoadingComponent: FunctionComponent = (): ReactElement => {
    return <div className="loading-component" data-testid="loading-spinner">
        <div className="spinner"/>
    </div>;
};

export default LoadingComponent;
