import React, { ChangeEvent, FunctionComponent, ReactElement, useState } from 'react';

interface InputNumberComponentProps {
    error?: string;
    label: string;
    setValue: Function;
    value?: number;
}

const InputNumberComponent: FunctionComponent<InputNumberComponentProps> = ({ error, label, setValue, value }): ReactElement => {
    const [ inputValue, setInputValue ] = useState<string>(String(value || ''));

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.value) {
            if (!Number.isNaN(Number(event.target.value))) {
                setInputValue(event.target.value);
                setValue(Number(event.target.value));
            }
        } else {
            setInputValue('');
            setValue(undefined);
        }
    }
    
    return <div className="margin-top">
        <label>{ label }</label>
        <input
            data-testid="input"
            onChange={handleChange}
            placeholder={label}
            value={inputValue || ''}
        />
        { error && <small className="error">{ error }</small>}
    </div>;
}

export default InputNumberComponent;
