import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import InputNumberComponent from './input-number.component';

describe('InputNumberComponent', () => {
    it('should render an input, call to setValue with the data parsed as a number when on change event is fired with a valid number, call to setValue with undefined when on change event is fired with empty data, do not call to setValue when on change event is fired with an invalid number and do not show an error when it is undefined', () => {
        const setValue = jest.fn();

        const { container, debug, getByTestId } = render(<InputNumberComponent label="Label" setValue={setValue} value={2} />);

        fireEvent.change(getByTestId('input'), { target: { value: '1' }});
        fireEvent.change(getByTestId('input'), { target: { value: 'R' }});
        fireEvent.change(getByTestId('input'), { target: { value: '' }});
        
        expect(setValue).toHaveBeenCalledWith(1);
        expect(setValue).toHaveBeenCalledWith(undefined);
        expect(setValue).toHaveBeenCalledTimes(2);
        expect(container.getElementsByClassName('error').length).toEqual(0);
    });

    it('should render an error when it is defined', () => {
        const { container } = render(<InputNumberComponent error="Error" label="Label" setValue={jest.fn()} />);

        expect(container.getElementsByClassName('error').length).toEqual(1);
    });
});
