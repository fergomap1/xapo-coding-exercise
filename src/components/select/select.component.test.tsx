import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import SelectComponent from './select.component';
import SelectOptionImp from 'model/select-option.imp';

describe('SelectComponent', () => {
    it('should render a select, call to setValue when on change event is fired and do not show an error when it is undefined', () => {
        const options = [new SelectOptionImp('BTC', 'BTC')];
        const setValue = jest.fn();

        const { container, getByTestId } = render(<SelectComponent label="Label" options={options} setValue={setValue} />);

        fireEvent.change(getByTestId('select'), options[0]);

        expect(setValue).toHaveBeenCalledWith(options[0]);
        expect(container.getElementsByClassName('error').length).toEqual(0);
    });

    it('should render an error when it is defined', () => {
        const { container } = render(<SelectComponent error="Error" label="Label" options={[]} setValue={jest.fn()} />);

        expect(container.getElementsByClassName('error').length).toEqual(1);
    });
});
