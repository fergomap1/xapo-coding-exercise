import SelectOption from 'model/select-option';
import React, { FunctionComponent, ReactElement } from 'react';
import Select from 'react-select';
import './select.component.scss';

interface SelectComponentProps {
    error?: string;
    label: string;
    options: SelectOption[];
    value?: SelectOption;
    setValue: Function;
}

const SelectComponent: FunctionComponent<SelectComponentProps> = ({ error, label, options, value, setValue }): ReactElement => {
    return <div className="select-component margin-top">
        <label>{label}</label>
        <Select
            classNamePrefix="select"
            isSearchable={false}
            onChange={v => setValue(v)}
            options={options}
            placeholder="Select"
            value={value}
        />
        {error && <small className="error">{error}</small>}
    </div>;
}

export default SelectComponent;
