/* istanbul ignore file */

interface AppConfig {
    ENDPOINTS: {
        RATES: string;
    };
    LOCAL_STORAGE_CALCULATIONS: string;
    MILLISECONDS_IN_SECOND: number;
    ROUTES: {
        EXCHANGE: string;
        EXCHANGE_DETAIL: string;
        RATES: string;
    };
}

export const APP_CONSTANTS: AppConfig = {
    ENDPOINTS: {
        RATES: 'https://openexchangerates.org/api/latest.json?app_id=fe31a65bc50544af94b161ee3d728079'
    },
    LOCAL_STORAGE_CALCULATIONS: 'xapo_exchange_calculations',
    MILLISECONDS_IN_SECOND: 1000,
    ROUTES: {
        EXCHANGE: '/exchange',
        EXCHANGE_DETAIL: '/exchange/:currencyCode',
        RATES: '/rates'
    }
};
