import React from 'react';
import { render } from '@testing-library/react';
import App from 'App';
import RatesInformationImp from 'model/rates-information.imp';
import useRates from 'hooks/rates.hook';

jest.mock('hooks/rates.hook', () => {
    return jest.fn(() => ({ errorMessage: '', isLoading: false, ratesInformation: new RatesInformationImp() }));
})

describe('AppComponent', () => {
    afterAll(() => {
        jest.resetModules();
    });

    it('should render the default route and do not render the loading spinner nor error when they are falsy', () => {
        const { container, queryByTestId } = render(<App/>);

        expect(container.getElementsByClassName('rates-component').length).toEqual(1);
        expect(queryByTestId('error-message')).toBeNull();
        expect(queryByTestId('loading-spinner')).toBeNull();
    });

    it('should render the loading spinner and the error when they are defined', () => {
        (useRates as jest.Mock).mockReturnValue({ errorMessage: 'Error', isLoading: true, ratesInformation: new RatesInformationImp() });
    
        const { getByTestId } = render(<App/>);

        expect(getByTestId('error-message')).toBeDefined();
        expect(getByTestId('loading-spinner')).toBeDefined();
    });
});
