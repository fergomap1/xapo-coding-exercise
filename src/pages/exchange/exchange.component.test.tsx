import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ExchangeComponent from './exchange.component';
import RatesInformationImp from 'model/rates-information.imp';
import { Route, Router, Switch } from 'react-router';
import { createMemoryHistory } from 'history';
import { APP_CONSTANTS } from 'config/app.config';
import SelectOptionImp from 'model/select-option.imp';

describe('ExchangeComponent', () => {

    beforeEach(() => {
        Object.defineProperty(window, 'localStorage', {
            value: {
                getItem: jest.fn().mockReturnValue(`[
                    { "createdAtTimestamp": 1619866027720, "currency": "BTC", "currencyAmount": 1, "dollarAmount": 0.2722495984318423, "timestamp" :1619863207 },
                    { "createdAtTimestamp": 1619866027721, "currency": "BTC", "currencyAmount": 1, "dollarAmount": 0.2722495984318423, "timestamp" :1619863207 },
                    { "createdAtTimestamp": 1619866027722, "currency": "CNB", "currencyAmount": 1, "dollarAmount": 0.2722495984318423, "timestamp" :1619863207 }
                ]`),
                setItem: jest.fn()
            }
        });
    });

    it('should render a Redirect when the route is the detail one and the currency queryParam is not in the rates object', () => {
        const history = createMemoryHistory();
        history.push(`${APP_CONSTANTS.ROUTES.EXCHANGE}/FALSE_CURRENCY`);

        render(<Router history={history}>
            <Switch>
                <Route path={APP_CONSTANTS.ROUTES.EXCHANGE} exact>
                    <ExchangeComponent ratesInformation={new RatesInformationImp({ BTC: 1 })} />
                </Route>
                <Route path={APP_CONSTANTS.ROUTES.EXCHANGE_DETAIL}>
                    <ExchangeComponent ratesInformation={new RatesInformationImp({ BTC: 1 })} />
                </Route>
            </Switch>
        </Router>);

        expect(history.location.pathname).toEqual(APP_CONSTANTS.ROUTES.EXCHANGE);
    });

    it('should render the complete form when it is not the detail route, load the data from localStorage and show all the calculations, show the errors when the form is not valid and add the calculation to localStorage when the form is submitted with valid data', () => {
        const history = createMemoryHistory();
        history.push(APP_CONSTANTS.ROUTES.EXCHANGE);

        const { container, getAllByTestId, getByTestId } = render(<Router history={history}><ExchangeComponent ratesInformation={new RatesInformationImp({ BTC: 0.00002, CNB: 1 })} /></Router>);

        expect(localStorage.getItem).toHaveBeenCalled();
        expect(getByTestId('select')).toBeDefined();

        fireEvent.submit(getByTestId('form'), { preventDefault: jest.fn() });

        expect(container.getElementsByClassName('error').length).toEqual(2);
        expect(container.getElementsByClassName('exchange-calculation-component').length).toEqual(3);

        fireEvent.change(getByTestId('input'), { target: { value: '1' } });
        fireEvent.change(getByTestId('select'), new SelectOptionImp('BTC', 'BTC'));
        fireEvent.submit(getByTestId('form'), { preventDefault: jest.fn() });

        expect(localStorage.setItem).toHaveBeenCalled();
        expect(container.getElementsByClassName('error').length).toEqual(0);
        expect(container.getElementsByClassName('exchange-calculation-component').length).toEqual(4);
        expect(getAllByTestId('calculation-info')[0].textContent).toEqual('1.00000000 BTC are 50,000.00000000 USD');
    });

    it('should render only the amount in the form when it is the detail route with a valid currency, show all the calculations of this currency, show the errors when the form is not valid, add the calculation to localStorage when the form is submitted with valid data and remove the element from the list when the button is clicked', () => {
        const history = createMemoryHistory();
        history.push(`${APP_CONSTANTS.ROUTES.EXCHANGE}/BTC`);

        const { container, getAllByTestId, getByTestId, queryByTestId } = render(<Router history={history}>
            <Route path={APP_CONSTANTS.ROUTES.EXCHANGE_DETAIL}>
                <ExchangeComponent ratesInformation={new RatesInformationImp({ BTC: 1, CNB: 1 })} />
            </Route>
        </Router>);

        expect(queryByTestId('select')).toBeNull();

        fireEvent.submit(getByTestId('form'), { preventDefault: jest.fn() });

        expect(container.getElementsByClassName('error').length).toEqual(1);
        expect(container.getElementsByClassName('exchange-calculation-component').length).toEqual(2);

        fireEvent.change(getByTestId('input'), { target: { value: '1' } });
        fireEvent.submit(getByTestId('form'), { preventDefault: jest.fn() });

        expect(container.getElementsByClassName('error').length).toEqual(0);
        expect(container.getElementsByClassName('exchange-calculation-component').length).toEqual(3);

        fireEvent.click(getAllByTestId('button')[1]);

        expect(localStorage.setItem).toHaveBeenCalled();
        expect(container.getElementsByClassName('exchange-calculation-component').length).toEqual(2);
    });
});
