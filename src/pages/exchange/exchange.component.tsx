import React, { FormEvent, FunctionComponent, ReactElement, useEffect, useState } from 'react';
import { Redirect, useLocation, useParams } from 'react-router-dom';
import InputNumberComponent from 'components/input-number/input-number.component';
import SelectComponent from 'components/select/select.component';
import ButtonComponent from 'components/button/button.component';
import ExchangeCalculationComponent from 'components/exchange-calculation/exchange-calculation.component';
import { APP_CONSTANTS } from 'config/app.config';
import ExchangeCalculation from 'model/exchange-calculation';
import ExchangeCalculationImp from 'model/exchange-calculation.imp';
import RatesInformation from 'model/rates-information';
import SelectOption from 'model/select-option';
import SelectOptionImp from 'model/select-option.imp';
import './exchange.component.scss';

interface ExchangeComponentProps {
    ratesInformation: RatesInformation;
}

const ExchangeComponent: FunctionComponent<ExchangeComponentProps> = ({ ratesInformation }): ReactElement => {
    const { currencyCode } = useParams<{ currencyCode: string }>();
    const { pathname } = useLocation();
    const [ amount, setAmount ] = useState<number>();
    const [ amountError, setAmountError ] = useState<string>('');
    const [ rate, setRate ] = useState<SelectOption>();
    const [ rateError, setRateError ] = useState<string>('');
    const [ exchangeCalculations, setExchangeCalculations ] = useState<ExchangeCalculation[]>(JSON.parse(localStorage.getItem(APP_CONSTANTS.LOCAL_STORAGE_CALCULATIONS) || '[]'));
    const [ redirectToExchange, setRedirectToExchange ] = useState<boolean>(false);

    useEffect(() => {
        // It should redirect when the rates information is loaded, the path is the exchange detail and the currency of the route is not defined or is not in the rates information
        setRedirectToExchange(
            !!Object.keys(ratesInformation.rates).length && 
            pathname !== APP_CONSTANTS.ROUTES.EXCHANGE && 
            (!currencyCode || !ratesInformation.rates[currencyCode])
        );
    }, [currencyCode, pathname, ratesInformation.rates]);
    
    const calculateExchange = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        const currency = currencyCode || rate?.value

        if (!amount || !currency) {
            setAmountError(amount ? '' : 'Required field');
            setRateError(rate?.value ? '' : 'Required field');
        } else {
            const newExchangeCalculations = [
                new ExchangeCalculationImp(
                    Date.now(), 
                    currency, 
                    amount, 
                    amount / ratesInformation.rates[currency],
                    ratesInformation.timestamp
                ),
                ...exchangeCalculations
            ];
            
            setAmountError('');
            setRateError('');
            setExchangeCalculations(newExchangeCalculations);
            localStorage.setItem(APP_CONSTANTS.LOCAL_STORAGE_CALCULATIONS, JSON.stringify(newExchangeCalculations));
        }
    };

    const deleteCalculation = (calculation: ExchangeCalculation): void => {
        const exchangeCalculationsCopy = [...exchangeCalculations];
        const index = exchangeCalculations.indexOf(calculation);

        if (index !== -1) {
            exchangeCalculationsCopy.splice(index, 1);
            setExchangeCalculations([...exchangeCalculationsCopy]);
            localStorage.setItem(APP_CONSTANTS.LOCAL_STORAGE_CALCULATIONS, JSON.stringify(exchangeCalculationsCopy));
        }
    };
    
    if (redirectToExchange) {
        return <Redirect to={APP_CONSTANTS.ROUTES.EXCHANGE} />;
    }

    return <section className="exchange-component">
        <h2>Exchange</h2>
        <form className="exchange-form" onSubmit={calculateExchange} noValidate={true} data-testid="form">
            <InputNumberComponent error={amountError} label="Amount" setValue={setAmount} value={amount} />
            { !currencyCode && <SelectComponent 
                                    error={rateError} 
                                    label="Cryptocurrency" 
                                    options={Object.keys(ratesInformation.rates).map((key: string) => new SelectOptionImp(key, key))} 
                                    setValue={setRate} 
                                    value={rate} 
                                /> 
            }
            <div className="margin-top">
                <ButtonComponent label="EXCHANGE" type="submit" />
            </div>
        </form>
        { exchangeCalculations
            .filter((calculation: ExchangeCalculation) => currencyCode ? calculation.currency === currencyCode : true)
            .map((calculation: ExchangeCalculation) => <ExchangeCalculationComponent 
                                                        calculation={calculation}
                                                        key={calculation.createdAtTimestamp} 
                                                        onDelete={deleteCalculation}
                                                    /> 
        )}
    </section>;
};

export default ExchangeComponent;
