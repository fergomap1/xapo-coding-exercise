import React, { FunctionComponent, ReactElement } from 'react';
import RatesInformation from 'model/rates-information';
import { APP_CONSTANTS } from 'config/app.config';
import { Link } from 'react-router-dom';
import './rates.component.scss';

interface RatesComponentProps {
    ratesInformation: RatesInformation;
}

const RatesComponent: FunctionComponent<RatesComponentProps> = ({ ratesInformation }): ReactElement => {
    return <section className="rates-component">
        <h2>Cryptocurrency rates</h2>
        {Object.keys(ratesInformation.rates).length ? <>
            <p className="primary" data-testid="last-update">Last update: {new Date(ratesInformation.timestamp * APP_CONSTANTS.MILLISECONDS_IN_SECOND).toUTCString()}</p>
            <table className="rates-table">
                <thead>
                    <tr>
                        <th>CryptoCurrency</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.keys(ratesInformation.rates).map((key: string) => <tr data-testid="table-row" key={key}>
                        <td><Link className="currency-link" to={`${APP_CONSTANTS.ROUTES.EXCHANGE}/${key}`}>{key}</Link></td>
                        <td className="dollars">{(1 / ratesInformation.rates[key]).toLocaleString('en-US', { maximumFractionDigits: 8, minimumFractionDigits: 8 })} USD</td>
                    </tr>)}
                </tbody>
            </table>
        </> : <p className="centered" data-testid="no-data">No data loaded yet!</p>}
    </section>;
};

export default RatesComponent;
