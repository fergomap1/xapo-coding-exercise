import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import RatesComponent from './rates.component';
import RatesInformationImp from 'model/rates-information.imp';
import { MemoryRouter } from 'react-router';

describe('RatesComponent', () => {
    it('should render a no data text when there are no rates', () => {
        const { getByTestId, queryByTestId } = render(<RatesComponent ratesInformation={new RatesInformationImp()} />);

        expect(queryByTestId('last-update')).toBeNull();
        expect(getByTestId('no-data')).toBeDefined();
    });

    it('should render the timestamp and a table with the rates when there are rates', () => {
        const { getAllByTestId, getByTestId, queryByTestId } = render(<MemoryRouter><RatesComponent ratesInformation={new RatesInformationImp({ BTC: 0.000017362848, ETH: 1 }, 1619784000)} /></MemoryRouter>);

        expect(queryByTestId('no-data')).toBeNull();
        expect(getByTestId('last-update').textContent).toEqual('Last update: Fri, 30 Apr 2021 12:00:00 GMT');
        expect(getAllByTestId('table-row').length).toEqual(2);
        expect(getAllByTestId('table-row')[0].textContent).toEqual('BTC57,594.23799598 USD');
        expect(getAllByTestId('table-row')[0].getElementsByTagName('a')[0]).toHaveAttribute('href', '/exchange/BTC');
    });
});
